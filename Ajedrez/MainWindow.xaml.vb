﻿Imports System.Windows.Threading

Class MainWindow
#Region "Declarando Variables"
    Dim _x As Integer = 0
    Dim _y As Integer = 0
    Dim _tableroMatriz(0 To 7, 0 To 7) As Object
    Dim _anteriorX As Integer
    Dim _anteriorY As Integer
    Dim _nuevaX As Integer = 0
    Dim _nuevaY As Integer = 0
    Dim _turno As Boolean = True
    Dim _objeto
    Dim _ganador As Boolean = False
    Dim colorPieza As String = "B"
    Dim _posicionReyBlancoX
    Dim _posicionReyBlancoY
    Private _horas As Integer
    Private _minutos As Integer
    Private _segundos As Integer
    Private _miliSegundos As Integer
    Private _startTiempo As Boolean
    Private _tiempo As New DispatcherTimer


#End Region

#Region "Eventos del Menu"

    Private Sub NuevoJuego_Click(sender As Object, e As RoutedEventArgs)
        MsgBox("Desea Iniciar un juego nuevo?", vbOKCancel)
        Tablero.Children.Clear()
        TableroPuesto()
        Erase _tableroMatriz
        ReDim _tableroMatriz(0 To 7, 0 To 7)
        PonerPiezas()
    End Sub
    Private Sub Salir_Click(sender As Object, e As RoutedEventArgs)
        Me.Close()
    End Sub

    Private Sub JugarBlancas_Click(sender As Object, e As RoutedEventArgs)

        Tablero.Children.Clear()
        TableroPuesto()
        Erase _tableroMatriz
        ReDim _tableroMatriz(0 To 7, 0 To 7)
        PonerPiezas()
        _turno = False
    End Sub

    Private Sub JugarNegras_Click(sender As Object, e As RoutedEventArgs)

        Tablero.Children.Clear()
        TableroPuesto()
        Erase _tableroMatriz
        ReDim _tableroMatriz(0 To 7, 0 To 7)
        PonerPiezas()
        _turno = True
    End Sub

#End Region

#Region "Incializacion del Tablero"
    Private Sub Tablero_Initialized(sender As Object, e As EventArgs)
        Tablero.Children.Clear()
        TableroPuesto()
        PonerPiezas()
        Cronometro()

    End Sub

    Private Sub TableroPuesto()
        Tablero.Children.Add(A1)
        Tablero.Children.Add(B1)
        Tablero.Children.Add(C1)
        Tablero.Children.Add(D1)
        Tablero.Children.Add(E1)
        Tablero.Children.Add(F1)
        Tablero.Children.Add(G1)
        Tablero.Children.Add(H1)

        Tablero.Children.Add(A2)
        Tablero.Children.Add(B2)
        Tablero.Children.Add(C2)
        Tablero.Children.Add(D2)
        Tablero.Children.Add(E2)
        Tablero.Children.Add(F2)
        Tablero.Children.Add(G2)
        Tablero.Children.Add(H2)

        Tablero.Children.Add(A3)
        Tablero.Children.Add(B3)
        Tablero.Children.Add(C3)
        Tablero.Children.Add(D3)
        Tablero.Children.Add(E3)
        Tablero.Children.Add(F3)
        Tablero.Children.Add(G3)
        Tablero.Children.Add(H3)

        Tablero.Children.Add(A4)
        Tablero.Children.Add(B4)
        Tablero.Children.Add(C4)
        Tablero.Children.Add(D4)
        Tablero.Children.Add(E4)
        Tablero.Children.Add(F4)
        Tablero.Children.Add(G4)
        Tablero.Children.Add(H4)

        Tablero.Children.Add(A5)
        Tablero.Children.Add(B5)
        Tablero.Children.Add(C5)
        Tablero.Children.Add(D5)
        Tablero.Children.Add(E5)
        Tablero.Children.Add(F5)
        Tablero.Children.Add(G5)
        Tablero.Children.Add(H5)

        Tablero.Children.Add(A6)
        Tablero.Children.Add(B6)
        Tablero.Children.Add(C6)
        Tablero.Children.Add(D6)
        Tablero.Children.Add(E6)
        Tablero.Children.Add(F6)
        Tablero.Children.Add(G6)
        Tablero.Children.Add(H6)

        Tablero.Children.Add(A7)
        Tablero.Children.Add(B7)
        Tablero.Children.Add(C7)
        Tablero.Children.Add(D7)
        Tablero.Children.Add(E7)
        Tablero.Children.Add(F7)
        Tablero.Children.Add(G7)
        Tablero.Children.Add(H7)

        Tablero.Children.Add(A8)
        Tablero.Children.Add(B8)
        Tablero.Children.Add(C8)
        Tablero.Children.Add(D8)
        Tablero.Children.Add(E8)
        Tablero.Children.Add(F8)
        Tablero.Children.Add(G8)
        Tablero.Children.Add(H8)

    End Sub

    Private Sub PonerPiezas()

        Dim _torreNegra1 As New TorreNegra
        Grid.SetColumn(_torreNegra1, 0)
        Grid.SetRow(_torreNegra1, 0)
        Tablero.Children.Add(_torreNegra1)
        _tableroMatriz(0, 0) = _torreNegra1

        Dim _caballoNegro1 As New CaballoNegro
        Grid.SetColumn(_caballoNegro1, 1)
        Grid.SetRow(_caballoNegro1, 0)
        Tablero.Children.Add(_caballoNegro1)
        _tableroMatriz(0, 1) = _caballoNegro1

        Dim _alfilNegro1 As New AlfilNegro
        Grid.SetColumn(_alfilNegro1, 2)
        Grid.SetRow(_alfilNegro1, 0)
        Tablero.Children.Add(_alfilNegro1)
        _tableroMatriz(0, 2) = _alfilNegro1

        Dim _reinaNegra As New ReinaNegra
        Grid.SetColumn(_reinaNegra, 3)
        Grid.SetRow(_reinaNegra, 0)
        Tablero.Children.Add(_reinaNegra)
        _tableroMatriz(0, 3) = _reinaNegra

        Dim _reyNegro As New ReyNegro
        Grid.SetColumn(_reyNegro, 4)
        Grid.SetRow(_reyNegro, 0)
        Tablero.Children.Add(_reyNegro)
        _tableroMatriz(0, 4) = _reyNegro

        Dim _alfilNegro2 As New AlfilNegro
        Grid.SetColumn(_alfilNegro2, 5)
        Grid.SetRow(_alfilNegro2, 0)
        Tablero.Children.Add(_alfilNegro2)
        _tableroMatriz(0, 5) = _alfilNegro2

        Dim _caballoNegro2 As New CaballoNegro
        Grid.SetColumn(_caballoNegro2, 6)
        Grid.SetRow(_caballoNegro2, 0)
        Tablero.Children.Add(_caballoNegro2)
        _tableroMatriz(0, 6) = _caballoNegro2

        Dim _torreNegra2 As New TorreNegra
        Grid.SetColumn(_torreNegra2, 7)
        Grid.SetRow(_torreNegra2, 0)
        Tablero.Children.Add(_torreNegra2)
        _tableroMatriz(0, 7) = _torreNegra2


        For i = 0 To 7 Step 1
            Dim _peonNegro As New PeonNegro
            Grid.SetRow(_peonNegro, 1)
            Grid.SetColumn(_peonNegro, i)
            Tablero.Children.Add(_peonNegro)
            _tableroMatriz(1, i) = _peonNegro
        Next


        Dim _torreBlanca1 As New TorreBlanca
        Grid.SetColumn(_torreBlanca1, 7)
        Grid.SetRow(_torreBlanca1, 7)
        Tablero.Children.Add(_torreBlanca1)
        _tableroMatriz(7, 7) = _torreBlanca1

        Dim _caballoBlanco1 As New CaballoBlanco
        Grid.SetColumn(_caballoBlanco1, 6)
        Grid.SetRow(_caballoBlanco1, 7)
        Tablero.Children.Add(_caballoBlanco1)
        ''            FILA  , COLUMNA
        _tableroMatriz(7, 6) = _caballoBlanco1

        Dim _alfilBlanco1 As New AlfilBlanco
        Grid.SetColumn(_alfilBlanco1, 5)
        Grid.SetRow(_alfilBlanco1, 7)
        Tablero.Children.Add(_alfilBlanco1)
        _tableroMatriz(7, 5) = _alfilBlanco1

        Dim _reyBlanco As New ReyBlanco
        Grid.SetColumn(_reyBlanco, 4)
        Grid.SetRow(_reyBlanco, 7)
        Tablero.Children.Add(_reyBlanco)
        _tableroMatriz(7, 4) = _reyBlanco

        Dim _reynaBlanca As New ReinaBlanca
        Grid.SetColumn(_reynaBlanca, 3)
        Grid.SetRow(_reynaBlanca, 7)
        Tablero.Children.Add(_reynaBlanca)
        _tableroMatriz(7, 3) = _reynaBlanca

        Dim _alfilBlanco2 As New AlfilBlanco
        Grid.SetColumn(_alfilBlanco2, 2)
        Grid.SetRow(_alfilBlanco2, 7)
        Tablero.Children.Add(_alfilBlanco2)
        _tableroMatriz(7, 2) = _alfilBlanco2

        Dim _caballoBlanco2 As New CaballoBlanco
        Grid.SetColumn(_caballoBlanco2, 1)
        Grid.SetRow(_caballoBlanco2, 7)
        Tablero.Children.Add(_caballoBlanco2)
        _tableroMatriz(7, 1) = _caballoBlanco2

        Dim _torreBlanca2 As New TorreBlanca
        Grid.SetColumn(_torreBlanca2, 0)
        Grid.SetRow(_torreBlanca2, 7)
        Tablero.Children.Add(_torreBlanca2)
        _tableroMatriz(7, 0) = _torreBlanca2


        For i = 0 To 7 Step 1
            Dim _peonBlanco As New PeonBlanco
            Grid.SetRow(_peonBlanco, 6)
            Grid.SetColumn(_peonBlanco, i)
            Tablero.Children.Add(_peonBlanco)
            _tableroMatriz(6, i) = _peonBlanco
        Next

    End Sub


#End Region

#Region "Eventos del Tablero  MouseMove"
    Private Sub Tablero_MouseMove(sender As Object, e As MouseEventArgs)
        _x = (e.GetPosition(sender).X \ 75)
        _y = (e.GetPosition(sender).Y \ 75)
        Coordenadas.Text = "Columna: " & _x + 1 & " - Fila: " & _y + 1

        Dim negro As String = "Negras"
        Dim blanco As String = "Blancas"

        If _turno = False Then
            nombrePieza.Text = ("Es el turno de las piezas: " & blanco)
        ElseIf _turno = True Then
            nombrePieza.Text = ("Es el turno de las piezas: " & negro)
        End If
    End Sub

#End Region

#Region "Metodos para mover piezas"
    Private Sub Control_Click(sender As Object, e As MouseButtonEventArgs)

        If _turno = True Then
            If Not IsNothing(_tableroMatriz(_y, _x)) And (_anteriorX = Nothing And _anteriorY = Nothing) Then

                _anteriorY = _y
                _anteriorX = _x
                _objeto = _tableroMatriz(_y, _x)
                Console.WriteLine("PRIMER CLICK")
            ElseIf Not IsNothing(_objeto) Then
                If IsNothing(_tableroMatriz(_y, _x)) Then

                    _nuevaX = _x
                    _nuevaY = _y

                    ComprobarPiezaNegra(_objeto)
                    Console.WriteLine("Segundo Click vacio poner pieza")

                    If _turno = True Then
                        errorMovimiento()
                    End If

                ElseIf Not IsNothing(_tableroMatriz(_y, _x)) Then
                    If _tableroMatriz(_anteriorY, _anteriorX).GetType.Name.ToString.Contains("N") And _tableroMatriz(_y, _x).GetType.Name.ToString.Contains("ReyBlanco") Then
                        _nuevaX = _x
                        _nuevaY = _y
                        matarNegras(_objeto)
                        MsgBox("Jaque Mate")
                        MsgBox("Ganaron las piezas negras")
                        MsgBox("Desea jugar otra vez", vbOKCancel)
                        System.Diagnostics.Process.Start(Application.ResourceAssembly.Location)
                        Application.Current.Shutdown()

                    ElseIf _tableroMatriz(_anteriorY, _anteriorX).GetType.Name.ToString.Contains("N") And _tableroMatriz(_y, _x).GetType.Name.ToString.Contains("N") Then
                        MsgBox("No colocó bien la pieza O no es su turno!")
                        Console.WriteLine("Buuuu!")
                        _objeto = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                    Else
                        errorMovimiento()
                    End If
                Else
                    errorMovimiento()
                End If
            Else
                errorMovimiento()
            End If


        ElseIf _turno = False Then
            If Not IsNothing(_tableroMatriz(_y, _x)) And (_anteriorX = Nothing And _anteriorY = Nothing) Then

                _anteriorY = _y
                _anteriorX = _x
                _objeto = _tableroMatriz(_y, _x)
                Console.WriteLine("PRIMER CLICK")
            ElseIf Not IsNothing(_objeto) And (_anteriorX <> _x Or _anteriorY <> _y) Then
                If IsNothing(_tableroMatriz(_y, _x)) Then
                    _nuevaX = _x
                    _nuevaY = _y

                    ComprobarPiezaBlanca(_objeto)
                    Console.WriteLine("Segundo Click vacio poner pieza")


                    If _turno = False Then
                        errorMovimiento()
                    End If


                ElseIf Not IsNothing(_tableroMatriz(_y, _x)) Then
                    If _tableroMatriz(_anteriorY, _anteriorX).GetType.Name.ToString.Contains("B") And _tableroMatriz(_y, _x).GetType.Name.ToString.Contains("ReyNegro") Then
                        _nuevaX = _x
                        _nuevaY = _y
                        matarNegras(_objeto)
                        MsgBox("Jaque Mate")
                        MsgBox("Ganaron las piezas blancas")
                        MsgBox("Desea jugar otra vez", vbOKCancel)
                        System.Diagnostics.Process.Start(Application.ResourceAssembly.Location)
                        Application.Current.Shutdown()

                    ElseIf _tableroMatriz(_anteriorY, _anteriorX).GetType.Name.ToString.Contains("B") And _tableroMatriz(_y, _x).GetType.Name.ToString.Contains("N") Then
                        _nuevaX = _x
                        _nuevaY = _y
                        matarBlancas(_objeto)


                    ElseIf _tableroMatriz(_anteriorY, _anteriorX).GetType.Name.ToString.Contains("B") And _tableroMatriz(_y, _x).GetType.Name.ToString.Contains("") Then
                        MsgBox("No colocó bien la pieza O no es su turno! xD")
                        Console.WriteLine("Buuu!")
                        _objeto = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing

                    Else
                        errorMovimiento()
                    End If
                Else
                    errorMovimiento()
                End If
            Else
                errorMovimiento()
            End If

        End If

    End Sub
#End Region

#Region "Comprobar piezasNegras"
    Private Sub ComprobarPiezaNegra(_pieza As Object)
        Dim tipoPieza As String = _pieza.GetType.Name.ToString()
        Select Case tipoPieza

            Case "PeonNegro"
                MovimientoPeonNegro()
            Case "TorreNegra"
                MovimientoTorreNegra()
            Case "AlfilNegro"
                MovimientoAlfilNegro()
            Case "ReinaNegra"
                MovimientoReinaNegra()
            Case "ReyNegro"
                MovimientoReyNegro()
            Case "CaballoNegro"
                MovimientoCaballoNegro()
        End Select
    End Sub
#End Region

#Region "Comprobar piezasBlancas"
    Private Sub ComprobarPiezaBlanca(_pieza As Object)
        Dim tipoPieza As String = _pieza.GetType.Name.ToString()
        Select Case tipoPieza

            Case "PeonBlanco"
                MovimientoPeonBlanco()
            Case "TorreBlanca"
                MovimientoTorreBlanca()
            Case "AlfilBlanco"
                MovimientoAlfilBlanco()
            Case "ReinaBlanca"
                MovimientoReinaBlanca()
            Case "ReyBlanco"
                MovimientoReyBlanco()
            Case "CaballoBlanco"
                MovimientoCaballoBlanco()
        End Select
    End Sub
#End Region

#Region "Matar piezas N/B"
    Private Sub matarNegras(_pieza As Object)
        Dim tipoPieza As String = _pieza.GetType.Name.ToString()

        Select Case tipoPieza

            Case "PeonNegro"
                matarPeonNegro()
            Case "TorreNegra"
                matarTorreNegra()
            Case "AlfilNegro"
                matarAlfilNegro()
            Case "ReinaNegra"
                matarReinaNegra()
            Case "ReyNegro"
                matarReyNegro()
            Case "CaballoNegro"
                matarCaballoNegro()
        End Select
    End Sub

    Private Sub matarBlancas(_pieza As Object)
        Dim tipoPieza As String = _pieza.GetType.Name.ToString()
        Select Case tipoPieza

            Case "PeonBlanco"
                matarPeonBlanco()
            Case "TorreBlanca"
                matarTorreBlanca()
            Case "AlfilBlanco"
                matarAlfilBlanco()
            Case "ReinaBlanca"
                matarReinaBlanca()
            Case "ReyBlanco"
                matarReyBlanco()
            Case "CaballoBlanco"
                matarCaballoBlanco()
        End Select
    End Sub
#End Region

#Region "Movimiento Piezas Negras"

    Private Sub MovimientoPeonNegro()

        If _anteriorY = 1 Then

            If _anteriorX <> _nuevaX Then
                MsgBox("Error intentar de nuevo!")
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            ElseIf _nuevaY > _anteriorY + 2 Then
                MsgBox("ERROR: Mover una o dos casillas!")
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            ElseIf _anteriorY + 1 = _nuevaY Or _anteriorY + 2 = _nuevaY Then
                _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                Grid.SetColumn(_temp, _nuevaX)
                Grid.SetRow(_temp, _nuevaY)
                _objeto = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = _temp
                _temp = Nothing
                _anteriorX = Nothing
                _anteriorY = Nothing
                _turno = False
            End If



        ElseIf _anteriorY + 1 = _nuevaY Then
            If _anteriorX <> _nuevaX Then
                MsgBox("No se puede mover a los lados!")
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            Else
                _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                Dim _temp = _tableroMatriz(_anteriorY + 1, _nuevaX)
                Grid.SetColumn(_temp, _nuevaX)
                Grid.SetRow(_temp, _nuevaY)
                _objeto = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = _temp
                _temp = Nothing
                _anteriorX = Nothing
                _anteriorY = Nothing
                _turno = False
            End If
        ElseIf _nuevaY > _anteriorY + 1 Then
            MsgBox("El movimiento no corresponde!")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        ElseIf _nuevaX <> _anteriorX Then
            MsgBox("No se puede mover peon a horizontal")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        ElseIf _nuevaY < _anteriorY Then
            MsgBox("No se puede mover peon para atras")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If

    End Sub

    Private Sub MovimientoTorreNegra()
        ''
        If (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then
            If _anteriorX < _nuevaX Then

                For i = _anteriorX + 1 To _nuevaX Step 1
                    If i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    ElseIf Not IsNothing(_tableroMatriz(_nuevaY, i)) And i <> _nuevaY Then
                        Exit For
                    End If
                Next
            Else

                For i = _anteriorX - 1 To _nuevaX Step -1
                    If i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    ElseIf Not IsNothing(_tableroMatriz(_nuevaY, i)) Then
                        Exit For
                    End If
                Next
            End If
            ''
            If _anteriorY < _nuevaY Then

                For i = _anteriorY + 1 To _nuevaY Step 1
                    If i = _nuevaY Then
                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    ElseIf Not IsNothing(_tableroMatriz(i, _nuevaX)) Then
                        Exit For
                    End If
                Next

            Else

                For i = _anteriorY - 1 To _nuevaY Step -1
                    If i = _nuevaY Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False

                    ElseIf Not IsNothing(_tableroMatriz(i, _nuevaX)) And i <> _nuevaX Then

                        Exit For
                    End If
                Next

            End If

        Else
            MsgBox("La torre no se pude mover en diagonal")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If

    End Sub

    Public Sub MovimientoAlfilNegro()
        Dim _recorridoX As Integer = Math.Abs(_nuevaX - _anteriorX)
        Dim _recorridoY As Integer = Math.Abs(_nuevaY - _anteriorY)

        If _recorridoX = _recorridoY Then
            If _anteriorX < _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step 1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX + i)) Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY + i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            ElseIf _anteriorX > _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step 1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX - i)) Then
                        MsgBox("Hay una pieza en el camino, es: " & _tableroMatriz(_anteriorY + i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            ElseIf _anteriorX > _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX - i)) Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY - i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            ElseIf _anteriorX < _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX + i)) Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY - i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next
            Else
                MsgBox("Solo se mueve en Diagonal!", vbOKOnly)
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            End If
        End If

    End Sub

    Public Sub MovimientoCaballoNegro()
        If (_nuevaY = _anteriorY + 2 Or _nuevaY = _anteriorY - 2) And (_nuevaX = _anteriorX + 1 Or _nuevaX = _anteriorX - 1) Then
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _objeto = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        ElseIf (_nuevaY = _anteriorY + 1 Or _nuevaY = _anteriorY - 1) And (_nuevaX = _anteriorX + 2 Or _nuevaX = _anteriorX - 2) Then
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _objeto = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        Else
            MsgBox("Movimiento del caballo tiene que ser en L")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If
    End Sub

    Private Sub MovimientoReinaNegra()

        Dim _recorridoX As Integer = Math.Abs(_nuevaX - _anteriorX)
        Dim _recorridoY As Integer = Math.Abs(_nuevaY - _anteriorY)

        If _recorridoX = _recorridoY Then

            If _anteriorX < _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step 1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX + i)) Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY + i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            ElseIf _anteriorX > _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step 1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX - i)) Then
                        MsgBox("Hay una pieza en el camino, es: " & _tableroMatriz(_anteriorY + i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            ElseIf _anteriorX > _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX - i)) Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY - i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            ElseIf _anteriorX < _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX + i)) Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY - i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            End If


        ElseIf (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then

            If _anteriorX < _nuevaX Then

                For i = _anteriorX + 1 To _nuevaX Step 1
                    If Not IsNothing(_tableroMatriz(_nuevaY, i)) Then
                        MsgBox("Hay una pieza en el camino, es " & _tableroMatriz(_nuevaY, i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next
            Else

                For i = _anteriorX - 1 To _nuevaX Step -1
                    If Not IsNothing(_tableroMatriz(_nuevaY, i)) Then
                        MsgBox("Hay una pieza en el camino, es " & _tableroMatriz(_nuevaY, i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next
            End If

            If _anteriorY < _nuevaY Then
                ''
                For i = _anteriorY + 1 To _nuevaY Step 1
                    If Not IsNothing(_tableroMatriz(i, _nuevaX)) Then
                        MsgBox("Hay una pieza en el camino, es ! " & _tableroMatriz(i, _nuevaX).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaY Then
                        ''
                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            Else
                For i = _anteriorY - 1 To _nuevaY Step -1
                    If Not IsNothing(_tableroMatriz(i, _nuevaX)) Then
                        MsgBox("Hay una pieza en el camino, es  " & _tableroMatriz(i, _nuevaX).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaY Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = False
                    End If
                Next

            End If



        Else
            MsgBox("La reina no se pueda saltar esa pieza")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If
    End Sub

    Public Sub MovimientoReyNegro()
        If Math.Abs(_nuevaY - _anteriorY) = 1 And Math.Abs(_nuevaX - _anteriorX) = 1 Then
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _objeto = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        ElseIf (_nuevaY = _anteriorY + 1) Or (_nuevaY = _anteriorY - 1) Or (_nuevaX = _anteriorX - 1) _
            Or (_nuevaX = _anteriorX + 1) Then
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _objeto = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        Else
            MsgBox("Solo se mueve Rey un espacio cada lado")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If
    End Sub

#End Region

#Region "Movimiento Piezas Blancas"

    Private Sub MovimientoPeonBlanco()
        If _anteriorY = 6 Then
            If _anteriorX <> _nuevaX Then
                MsgBox("Error intentar de nuevo!")
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            ElseIf _nuevaY < _anteriorY - 2 Then
                MsgBox("ERROR: Mover una o dos casillas!")
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            ElseIf _anteriorY - 1 = _nuevaY Or _anteriorY - 2 = _nuevaY Then
                _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                Grid.SetColumn(_temp, _nuevaX)
                Grid.SetRow(_temp, _nuevaY)
                _objeto = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = _temp
                _temp = Nothing
                _anteriorX = Nothing
                _anteriorY = Nothing
                _turno = True
            End If


        ElseIf _anteriorY - 1 = _nuevaY Then
            If _anteriorX <> _nuevaX Then
                MsgBox("No se puede mover a los lados!")
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            Else
                _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                Dim _temp = _tableroMatriz(_anteriorY - 1, _nuevaX)
                Grid.SetColumn(_temp, _nuevaX)
                Grid.SetRow(_temp, _nuevaY)
                _objeto = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                _tableroMatriz(_nuevaY, _nuevaX) = _temp
                _temp = Nothing
                _anteriorX = Nothing
                _anteriorY = Nothing
                _turno = True
            End If
        ElseIf _nuevaY < _anteriorY - 1 Then
            MsgBox("El movimiento no corresponde!")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        ElseIf _nuevaX <> _anteriorX Then
            MsgBox("No se puede mover peon a horizontal")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        ElseIf _nuevaY > _anteriorY Then
            MsgBox("No se puede mover peon para atras")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing

        End If

    End Sub

    Private Sub MovimientoTorreBlanca()

        If (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then

            If _anteriorX < _nuevaX Then

                For i = _anteriorX + 1 To _nuevaX Step 1
                    If Not IsNothing(_tableroMatriz(_nuevaY, i)) And i <> _nuevaX Then
                        MsgBox("Hay una pieza en el camino, es:" & _tableroMatriz(_nuevaY, i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next
            Else

                For i = _anteriorX - 1 To _nuevaX Step -1
                    If Not IsNothing(_tableroMatriz(_nuevaY, i)) And i <> _nuevaX Then
                        MsgBox("Hay una pieza en el camino, es:" & _tableroMatriz(_nuevaY, i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next
            End If

            If _anteriorY < _nuevaY Then

                For i = _anteriorY + 1 To _nuevaY Step 1
                    If Not IsNothing(_tableroMatriz(i, _nuevaX)) And i <> _nuevaY Then
                        MsgBox("Hay una pieza en el camino, es:" & _tableroMatriz(i, _nuevaX).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaY Then
                        ''
                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            Else

                For i = _anteriorY - 1 To _nuevaY Step -1
                    If Not IsNothing(_tableroMatriz(i, _nuevaX)) And i <> _nuevaY Then
                        MsgBox("Hay una pieza en el camino, es: " & _tableroMatriz(i, _nuevaX).GetType.Name.ToString)
                        Exit For
                    ElseIf i = _nuevaY Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            End If

        Else
            MsgBox("La torre no se pude mover en diagonal")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If


    End Sub

    Public Sub MovimientoAlfilBlanco()

        Dim _recorridoX As Integer = Math.Abs(_nuevaX - _anteriorX)
        Dim _recorridoY As Integer = Math.Abs(_nuevaY - _anteriorY)

        If _recorridoX = _recorridoY Then

            If _anteriorX < _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step 1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX + i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es: 1 " & _tableroMatriz(_anteriorY + i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next
                '
            ElseIf _anteriorX > _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step -1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX - i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es: 2 " & _tableroMatriz(_anteriorY + i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            ElseIf _anteriorX > _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX - i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es: 3 " & _tableroMatriz(_anteriorY - i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            ElseIf _anteriorX < _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX + i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY - i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next
            Else
                MsgBox("Solo se mueve en Diagonal!", vbOKOnly)
                _tableroMatriz(_nuevaY, _nuevaX) = Nothing
                _objeto = Nothing
            End If
        End If

    End Sub

    Public Sub MovimientoCaballoBlanco()
        If (_nuevaY = _anteriorY + 2 Or _nuevaY = _anteriorY - 2) And (_nuevaX = _anteriorX + 1 Or _nuevaX = _anteriorX - 1) Then
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _objeto = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = True
        ElseIf (_nuevaY = _anteriorY + 1 Or _nuevaY = _anteriorY - 1) And (_nuevaX = _anteriorX + 2 Or _nuevaX = _anteriorX - 2) Then
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _objeto = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = True
        Else
            MsgBox("Movimiento del caballo tiene que ser en L")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If
    End Sub

    Private Sub MovimientoReinaBlanca()


        Dim _recorridoX As Integer = Math.Abs(_nuevaX - _anteriorX)
        Dim _recorridoY As Integer = Math.Abs(_nuevaY - _anteriorY)

        If _recorridoX = _recorridoY Then

            If _anteriorX < _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step 1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX + i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY + i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            ElseIf _anteriorX > _nuevaX And _anteriorY < _nuevaY Then
                For i = 1 To _recorridoX Step 1
                    If Not IsNothing(_tableroMatriz(_anteriorY + i, _anteriorX - i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es: " & _tableroMatriz(_anteriorY + i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then
                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            ElseIf _anteriorX > _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX - i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY - i, _anteriorX - i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            ElseIf _anteriorX < _nuevaX And _anteriorY > _nuevaY Then
                For i = 1 To _recorridoX
                    If Not IsNothing(_tableroMatriz(_anteriorY - i, _anteriorX + i)) And i <> _recorridoX Then
                        MsgBox("Hay una pieza en el camino, es:  " & _tableroMatriz(_anteriorY - i, _anteriorX + i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _recorridoX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(_temp, _nuevaY)
                        Grid.SetColumn(_temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        _temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            End If


        ElseIf (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then

            If _anteriorX < _nuevaX Then

                For i = _anteriorX + 1 To _nuevaX Step 1
                    If Not IsNothing(_tableroMatriz(_nuevaY, i)) And i <> _nuevaX Then
                        MsgBox("Hay una pieza en el camino, es " & _tableroMatriz(_nuevaY, i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next
            Else

                For i = _anteriorX - 1 To _nuevaX Step -1
                    If Not IsNothing(_tableroMatriz(_nuevaY, i)) And i <> _nuevaX Then
                        MsgBox("Hay una pieza en el camino, es " & _tableroMatriz(_nuevaY, i).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaX Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next
            End If

            If _anteriorY < _nuevaY Then
                ''
                For i = _anteriorY + 1 To _nuevaY Step 1
                    If Not IsNothing(_tableroMatriz(i, _nuevaX)) And i <> _nuevaY Then
                        MsgBox("Hay una pieza en el camino, es ! " & _tableroMatriz(i, _nuevaX).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaY Then
                        ''
                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            Else
                For i = _anteriorY - 1 To _nuevaY Step -1
                    If Not IsNothing(_tableroMatriz(i, _nuevaX)) And i <> _nuevaY Then
                        MsgBox("Hay una pieza en el camino, es  " & _tableroMatriz(i, _nuevaX).GetType.Name.ToString, vbOKOnly)
                        Exit For
                    ElseIf i = _nuevaY Then

                        _tableroMatriz(_nuevaY, _nuevaX) = _objeto
                        Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
                        Grid.SetRow(temp, _nuevaY)
                        Grid.SetColumn(temp, _nuevaX)
                        _objeto = Nothing
                        _tableroMatriz(_anteriorY, _anteriorX) = Nothing
                        temp = Nothing
                        _anteriorX = Nothing
                        _anteriorY = Nothing
                        _turno = True
                    End If
                Next

            End If



        Else
            MsgBox("La reina no se pueda saltar esa pieza")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If



    End Sub

    Public Sub MovimientoReyBlanco()
        If Math.Abs(_nuevaY - _anteriorY) = 1 And Math.Abs(_nuevaX - _anteriorX) = 1 Then
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _objeto = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = True
        ElseIf (_nuevaY = _anteriorY + 1) Or (_nuevaY = _anteriorY - 1) Or (_nuevaX = _anteriorX - 1) _
            Or (_nuevaX = _anteriorX + 1) Then
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetRow(temp, _nuevaY)
            Grid.SetColumn(temp, _nuevaX)
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _objeto = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = True
        Else
            MsgBox("Solo se mueve Rey un espacio cada lado")
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _objeto = Nothing
        End If
    End Sub

#End Region

#Region "Comer piezasNegras"

    Private Sub matarPeonNegro()
        If (_anteriorY + 1 = _nuevaY And _anteriorX + 1 = _nuevaX) Or (_anteriorY + 1 = _nuevaY And _anteriorX - 1 = _nuevaX) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))

            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        Else
            errorMovimiento()
        End If

    End Sub

    Private Sub matarTorreNegra()

        If (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then
            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))

            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()
        End If

    End Sub

    Private Sub matarAlfilNegro()

        If (_anteriorX < _nuevaX And _anteriorY < _nuevaY) Or (_anteriorX > _nuevaX And _anteriorY < _nuevaY) Or
            (_anteriorX > _nuevaX And _anteriorY > _nuevaY) Or (_anteriorX < _nuevaX And _anteriorY > _nuevaY) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        Else
            errorMovimiento()

        End If

    End Sub

    Private Sub matarCaballoNegro()

        If (_nuevaY = _anteriorY + 2 Or _nuevaY = _anteriorY - 2) And (_nuevaX = _anteriorX + 1 Or _nuevaX = _anteriorX - 1) Or
            (_nuevaY = _anteriorY + 1 Or _nuevaY = _anteriorY - 1) And (_nuevaX = _anteriorX + 2 Or _nuevaX = _anteriorX - 2) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()

        End If
    End Sub

    Private Sub matarReinaNegra()

        If (_anteriorX < _nuevaX And _anteriorY < _nuevaY) Or (_anteriorX > _nuevaX And _anteriorY < _nuevaY) Or
          (_anteriorX > _nuevaX And _anteriorY > _nuevaY) Or (_anteriorX < _nuevaX And _anteriorY > _nuevaY) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        ElseIf (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then
            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()

        End If

    End Sub
    Private Sub matarReyNegro()

        If (Math.Abs(_nuevaY - _anteriorY) = 1 And Math.Abs(_nuevaX - _anteriorX) = 1) Or
            ((_nuevaY = _anteriorY + 1) Or (_nuevaY = _anteriorY - 1) Or (_nuevaX = _anteriorX - 1) _
            Or (_nuevaX = _anteriorX + 1)) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()

        End If

    End Sub

#End Region

#Region "Comer piezasBlancas"
    Private Sub matarPeonBlanco()
        If (_anteriorY + 1 = _nuevaY And _anteriorX + 1 = _nuevaX) Or (_anteriorY + 1 = _nuevaY And _anteriorX + 1 = _nuevaX) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))

            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        Else
            errorMovimiento()
        End If

    End Sub

    Private Sub matarTorreBlanca()

        If (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then
            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))

            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()
        End If

    End Sub

    Private Sub matarAlfilBlanco()

        If (_anteriorX < _nuevaX And _anteriorY < _nuevaY) Or (_anteriorX > _nuevaX And _anteriorY < _nuevaY) Or
            (_anteriorX > _nuevaX And _anteriorY > _nuevaY) Or (_anteriorX < _nuevaX And _anteriorY > _nuevaY) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False
        Else
            errorMovimiento()

        End If

    End Sub

    Private Sub matarCaballoBlanco()

        If (_nuevaY = _anteriorY + 2 Or _nuevaY = _anteriorY - 2) And (_nuevaX = _anteriorX + 1 Or _nuevaX = _anteriorX - 1) Or
            (_nuevaY = _anteriorY + 1 Or _nuevaY = _anteriorY - 1) And (_nuevaX = _anteriorX + 2 Or _nuevaX = _anteriorX - 2) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()

        End If
    End Sub

    Private Sub matarReinaBlanca()

        If (_anteriorX < _nuevaX And _anteriorY < _nuevaY) Or (_anteriorX > _nuevaX And _anteriorY < _nuevaY) Or
          (_anteriorX > _nuevaX And _anteriorY > _nuevaY) Or (_anteriorX < _nuevaX And _anteriorY > _nuevaY) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        ElseIf (_anteriorY = _nuevaY) Or (_anteriorX = _nuevaX) Then
            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()

        End If

    End Sub

    Private Sub matarReyBlanco()

        If (Math.Abs(_nuevaY - _anteriorY) = 1 And Math.Abs(_nuevaX - _anteriorX) = 1) Or
            ((_nuevaY = _anteriorY + 1) Or (_nuevaY = _anteriorY - 1) Or (_nuevaX = _anteriorX - 1) _
            Or (_nuevaX = _anteriorX + 1)) Then

            Tablero.Children.Remove(_tableroMatriz(_nuevaY, _nuevaX))
            _tableroMatriz(_nuevaY, _nuevaX) = _objeto
            Dim _temp = _tableroMatriz(_nuevaY, _nuevaX)
            Grid.SetColumn(_temp, _nuevaX)
            Grid.SetRow(_temp, _nuevaY)
            _objeto = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = Nothing
            _tableroMatriz(_anteriorY, _anteriorX) = Nothing
            _tableroMatriz(_nuevaY, _nuevaX) = _temp
            _temp = Nothing
            _anteriorX = Nothing
            _anteriorY = Nothing
            _turno = False

        Else
            errorMovimiento()

        End If

    End Sub
#End Region

#Region "Error en Movimiento"

    Private Sub errorMovimiento()
        MsgBox("No colocó la pieza bien O no es su turno! :V")
        MsgBox("Intentelo nuevamente")
        Console.WriteLine("Buuu!")
        _tableroMatriz(_nuevaX, _nuevaY) = Nothing
        _objeto = Nothing
        _anteriorX = Nothing
        _anteriorY = Nothing
    End Sub

#End Region

#Region "Jaque"
    Private Sub Jaque()

        If _posicionReyBlancoX >= 0 And _posicionReyBlancoX < 7 And _posicionReyBlancoY >= 0 And _posicionReyBlancoY < 7 Then
            If Not IsNothing(_tableroMatriz(_posicionReyBlancoY - 1, _posicionReyBlancoX + 1)) Then
                If (_tableroMatriz(_posicionReyBlancoY - 1, _posicionReyBlancoX + 1).GetType.Name.ToString.Contains("PeonNegro")) Then
                    MsgBox("Existe Jaque", vbOKOnly)
                    'Return True
                End If
            End If
        End If


        If _posicionReyBlancoX > 0 And _posicionReyBlancoX < 8 And _posicionReyBlancoY >= 1 And _posicionReyBlancoY < 8 Then
            If Not IsNothing(_tableroMatriz(_posicionReyBlancoY - 1, _posicionReyBlancoX - 1)) Then
                If (_tableroMatriz(_posicionReyBlancoY - 1, _posicionReyBlancoX - 1).GetType.Name.ToString.Contains("PeonNegro")) Then
                    MsgBox("Existe Jaque", vbOKOnly)
                    ' Return True
                End If
            End If
        End If



        'recta
        If _posicionReyBlancoX >= 0 And _posicionReyBlancoX < 8 Then
            For i = 1 To 7
                If _posicionReyBlancoX + i = 8 Then
                    Exit For
                ElseIf Not IsNothing(_tableroMatriz(_posicionReyBlancoY, _posicionReyBlancoX + i)) Then
                    If _tableroMatriz(_posicionReyBlancoY, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("TorreNegra") Or
                        _tableroMatriz(_posicionReyBlancoY, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("ReinaNegra") Then
                        MsgBox("Existe Jaque", vbOKOnly)
                        'Return True
                        Exit For

                    ElseIf _tableroMatriz(_posicionReyBlancoY, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("B") Or
                        _tableroMatriz(_posicionReyBlancoY, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("B") Then
                        Exit For
                    ElseIf _tableroMatriz(_posicionReyBlancoY, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("N") Or
                        _tableroMatriz(_posicionReyBlancoY, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("N") Then
                        Exit For
                    End If
                End If
            Next

        End If


        'diagonal
        If _posicionReyBlancoX >= 0 And _posicionReyBlancoX < 7 And _posicionReyBlancoY >= 0 And _posicionReyBlancoY < 7 Then
            For i = 1 To 7
                If _posicionReyBlancoX + i = 8 Or _posicionReyBlancoY + i = 8 Then
                    Exit For
                ElseIf Not IsNothing(_tableroMatriz(_posicionReyBlancoY + i, _posicionReyBlancoX + i)) Then
                    If _tableroMatriz(_posicionReyBlancoY + i, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("AlfilNegra") Or
                        _tableroMatriz(_posicionReyBlancoY + i, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("ReinaNegra") Then
                        MsgBox("Existe Jaque", vbOKOnly)
                        'Return True
                        Exit For

                    ElseIf _tableroMatriz(_posicionReyBlancoY + i, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("B") Or
                        _tableroMatriz(_posicionReyBlancoY + i, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("B") Then
                        Exit For
                    ElseIf _tableroMatriz(_posicionReyBlancoY + i, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("N") Or
                        _tableroMatriz(_posicionReyBlancoY + i, _posicionReyBlancoX + i).GetType.Name.ToString.Contains("N") Then
                        Exit For
                    End If
                End If
            Next

        End If
    End Sub

#End Region
    'Falta el jaque solo hay Jaque Mate si se come el Rey.
#Region "Timer"
    Private Sub Cronometro()
        If _startTiempo = False Then
            _tiempo.Interval = New TimeSpan(0, 0, 0, 0, 10)
            AddHandler _tiempo.Tick, AddressOf Intervalo
            _tiempo.Start()
            _startTiempo = True
        End If
    End Sub

    Private Sub Intervalo()
        _miliSegundos += 1
        If Me._miliSegundos >= 99 Then
            _miliSegundos = 0
            _segundos += 1
        End If
        If _segundos = 60 Then
            _segundos = 0
            _minutos += 1
        End If
        If _minutos = 60 Then
            _minutos = 0
            _horas += 1
        End If
        TiempoLabel.Content = Format(_horas, "00") & ":" & Format(_minutos, "00") & ":" & Format(_segundos, "00") & "." & Format(_miliSegundos, "00")
    End Sub
#End Region
End Class

